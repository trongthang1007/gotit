package com.example.gotit;


import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    private TessBaseAPI m_tess;
    Bitmap bitmapImage;
    String train = "vie";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initImageView();
        try {
            prepareLanguageDir();
            m_tess = new TessBaseAPI();
            m_tess.init(String.valueOf(getFilesDir()), train);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initImageView() {
        ImageView imgView = findViewById(R.id.img_input);
        bitmapImage = BitmapFactory.decodeResource(getResources(), R.drawable.img);
        imgView.setImageBitmap(bitmapImage);
    }

    private void copyFile() throws IOException {
        AssetManager assMng = getAssets();
        InputStream is = assMng.open("tessdata/" + train + ".traineddata");
        OutputStream os = new FileOutputStream(getFilesDir() + "/tessdata/" + train + ".traineddata");
        byte[] buffer = new byte[1024];
        int read;
        while ((read = is.read(buffer)) != -1) {
            os.write(buffer, 0, read);
        }

        is.close();
        os.flush();
        os.close();
    }

    private void prepareLanguageDir() throws IOException {
        File dir = new File(getFilesDir() + "/tessdata");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File trainedData = new File(getFilesDir() + "/tessdata/" + train + ".traineddata");
        if (!trainedData.exists()) {
            copyFile();
        }
    }

    public void doRecognize(View view) {
        if (m_tess == null) {
            return;
        }

        try {
            m_tess.setImage(bitmapImage);
            String result = m_tess.getUTF8Text();
            TextView resultView = findViewById(R.id.txt_result);
            resultView.setText(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
